from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.contrib.auth.models import User

from taplink.models import MyUser


class LoginForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].label = 'Номер телефона'
        self.fields['password'].label = 'Пароль'

    def clean(self):
        username = self.cleaned_data['username']
        password = self.cleaned_data['password']
        if not MyUser.objects.filter(username=username).exists():
            raise forms.ValidationError(f'Пользователь с номером {username} не найден')
        user = MyUser.objects.filter(username=username).first()
        if user:
            if not user.check_password(password):
                raise forms.ValidationError(f'Неверный пароль')
        return self.cleaned_data

    class Meta:
        model = MyUser
        fields = ['username', 'password']


class RegisterUserForm(UserCreationForm):
    class Meta:
        model = MyUser
        fields = ('first_name', 'last_name', 'username', 'date_joined', 'password')
