from django.contrib.auth.models import AbstractUser
from django.db import models


class MyUser(AbstractUser):
    EMAIL_FIELD = None
    username = models.CharField(max_length=15, null=True, blank=True, unique=True, verbose_name="Номер телефона")
    joined = models.DateField(auto_now_add=True, verbose_name='День рождения')
    first_name = models.CharField(max_length=200, null=True, blank=True, verbose_name='Имя')
    last_name = models.CharField(max_length=200, null=True, blank=True, verbose_name='Фамилия')
    is_staff = models.BooleanField(default=False, verbose_name='Прошел верификацию')

    def __str__(self):
        return self.username

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = "Пользователи"