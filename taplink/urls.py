from django.urls import path, include

from taplink.views import MyView, LoginView, RegisterView

urlpatterns = [
    path('', MyView.as_view(), name='view'),
    path('login/', LoginView.as_view(), name='login'),
    path('register/', RegisterView.as_view(), name='login')
]
