from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views import View

from taplink.forms import LoginForm, RegisterUserForm


class MyView(View):
    def get(self, request):
        context = User.is_authenticated
        return render(request, "taplink/base.html", {'auth': context})


class LoginView(View):
    def get(self, request):
        form = LoginForm(request.POST or None)
        context = {'form': form}
        return render(request, 'taplink/login.html', context)

    def post(self, request, *args, **kwargs):
        form = LoginForm(request.POST or None)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user:
                login(request, user)
                return HttpResponseRedirect('/')
        return render(request, 'taplink/login.html', {'form': form})


class RegisterView(View):
    def get(self, request):
        form = RegisterUserForm(request.POST or None)
        context = {'form': form}
        return render(request, 'taplink/register.html', context)

    def post(self, request, *args, **kwargs):
        form = RegisterUserForm(request.POST or None)
        if form.is_valid():
            new_user = form.save(commit=False)
            new_user.first_name = form.cleaned_data['first_name']
            new_user.last_name = form.cleaned_data['last_name']
            new_user.username = form.cleaned_data['username']
            new_user.date_joined = form.cleaned_data['date_joined']
            new_user.save()
            new_user.set_password(form.cleaned_data['password'])
            new_user.save()
            user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
            if user:
                login(request, user)
                return HttpResponseRedirect('/')
        context = {'form': form}
        return render(request, 'taplink/login.html', context)