from django.contrib import admin

# Register your models here.
from taplink.models import MyUser

admin.site.register(MyUser)